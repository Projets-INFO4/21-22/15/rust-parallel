extern crate tokio;
use log::debug;
use std::fmt;
use std::process;
use std::process::Stdio;
use std::thread;
use tokio::io::AsyncWriteExt;
use tokio::process::Command;

/**
 * Representation of the command execution environment :
 * - `cmd : String` - linux command name
 * - `parameter: Vec<String>` - list of command parameters
 * # Example
 * ```rust
 * use rust_parallel::core::job::Job;
 * let args: Vec<String> = vec![
 *             String::from("echo"),
 *             String::from("Hello"),
 *             String::from("World"),
 *         ];
 * let mut job : Job = Job::new(args, None);
 * job.exec();
 * ```
 */
pub struct Job {
    cmd: String,
    parameter: Vec<String>,
    input: Option<Vec<u8>>, // The str reference live as long as the job live
}

/***
 * Allow to display all information about the current job.
 */
impl fmt::Display for Job {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let _r = write!(
            f,
            r"{}{}{}",
            self.cmd,
            self.parameter
                .iter()
                .fold(String::new(), |acc, arg| acc + " " + arg),
            String::from_utf8(self.input.clone().unwrap_or(Vec::new())).unwrap()
        );
        Ok(())
    }
}

impl Job {
    /**
     * Return a new job set with the given parameter.
     * # Attributs
     * - `args: Vec<String>` - list of word use in the command
     * - `input_bytes: Option<Vec<u8>>` - block of input passed through the pipe
     */
    pub fn new(args: Vec<String>, input: Option<Vec<u8>>) -> Job {
        // the first element of the list is the linux command name
        let cmd = args[0].clone();

        // the others element of the list are the command parameters
        let mut parameter: Vec<String> = vec![];
        for i in 1..args.len() {
            parameter.push(args[i].clone());
        }

        Job { cmd, parameter, input }
    }

    /**
     * Execute the current job.
     * # Return
     * either the standard output if the execution was successful or an error.
     */
    pub async fn exec(&mut self) -> Result<process::Output, std::io::Error> {
        debug!("{} {:?}", process::id(), thread::current().id());

        // Create a new tokio command with the linux command name
        let mut command: Command = Command::new(self.cmd.clone());

        // Add parameters to the command
        for arg in &self.parameter {
            command.arg(&arg.clone());
        }

        // Specifying that we want to pipe the input
        command.stdin(Stdio::piped());
        // Pipe the output to control it (and the order)
        command.stdout(Stdio::piped());

        let mut child = command
            .spawn()
            .expect("failed to spawn command");

        debug!("<{}> spawn", self);

        let mut stdin = child
            .stdin
            .take()
            .expect("child did not have a handle to stdin");

        // Convert the string input as bytes
        let input_as_bytes = self
            .input
            .clone()
            .unwrap_or(Vec::new());

        // Write the block of input in the child process
        stdin
            .write_all(input_as_bytes.as_slice())
            .await
            .expect("could not write to stdin");

        // We drop the handle here which signals EOF to the child process.
        // This tells the child process that it there is no more data on the pipe.
        drop(stdin);

        // A future is a value that may not have finished computing yet.
        // This kind of "asynchronous value" makes it possible for a thread to continue doing useful work
        // while it waits for the value to become available.
        let future = child.wait_with_output();

        // Wait for the result of the command execution
        let future_result = future.await;
        match future_result {
            Err(e) => return Err(e),
            Ok(o) => return Ok(o),
        }

        // if there was no error during execution then the output is returned
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tokio::process::Child;
    use tokio::runtime::{Builder, Runtime};

    fn init(nb_thread: Option<usize>) -> Runtime {
        let mut runtime_builder: Builder = Builder::new_multi_thread();
        runtime_builder.enable_all();
        let runtime: Runtime = match nb_thread {
            None => runtime_builder.build().unwrap(),
            Some(n) => runtime_builder.worker_threads(n).build().unwrap(),
        };

        runtime
    }

    #[test]
    fn job_cmd1() {
        let _ = env_logger::builder().is_test(true).try_init();

        let runtime = init(Some(5));

        runtime.block_on(async {
            let _cmd: Child = Command::new(String::from("echo"))
                .arg(String::from("Hello World"))
                .spawn()
                .unwrap();
        });
    }

    #[test]
    #[should_panic]
    fn job_cmd2() {
        let _ = env_logger::builder().is_test(true).try_init();

        let runtime = init(Some(5));

        runtime.block_on(async {
            let _cmd: Child = Command::new(String::from("echo Hello World"))
                .spawn()
                .unwrap();
        });
    }

    #[test]
    #[should_panic]
    fn job_cmd3() {
        let _ = env_logger::builder().is_test(true).try_init();

        let runtime = init(Some(5));

        runtime.block_on(async {
            let _cmd: Child = Command::new(String::from("unknown_cmd")).spawn().unwrap();
        });
    }
}
